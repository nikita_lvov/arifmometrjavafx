package calculator;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import calculator.anim.Shake;

@SuppressWarnings("unused")
public class ControllerCalculator {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField firstNumber;

    @FXML
    private TextField result;

    @FXML
    private TextField secondNumber;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonSub;

    @FXML
    private Button buttonMultiplication;

    @FXML
    private Button buttonDivision;

    @FXML
    private Button buttonClear;

    @FXML
    void add(ActionEvent event) {
        animation();

        double first = Double.parseDouble(firstNumber.getText());
        double second = Double.parseDouble(secondNumber.getText());
        double sum = first + second;
        result.setText(String.valueOf(sum));
    }

    @FXML
    void division(ActionEvent event) {
        animation();

        double first = Double.parseDouble(firstNumber.getText());
        double second = Double.parseDouble(secondNumber.getText());
        if (second == 0) {
            result.setText("На 0 нельзя делить");
            return;
        }
        double sum = first / second;
        result.setText(String.valueOf(sum));
    }

    @FXML
    void multiplication(ActionEvent event) {
        animation();

        double first = Double.parseDouble(firstNumber.getText());
        double second = Double.parseDouble(secondNumber.getText());
        double sum = first * second;
        result.setText(String.valueOf(sum));
    }

    @FXML
    void sub(ActionEvent event) {
        animation();


        double first = Double.parseDouble(firstNumber.getText());
        double second = Double.parseDouble(secondNumber.getText());
        double sum = first - second;
        result.setText(String.valueOf(sum));
    }

    @FXML
    void clear(ActionEvent event) {
        firstNumber.clear();
        secondNumber.clear();
        result.clear();
    }

    void animation() {
        if (firstNumber.getText().isEmpty() || secondNumber.getText().isEmpty()) {
            Shake operandShake = (firstNumber.getText().isEmpty()) ? new Shake(firstNumber) : new Shake(secondNumber);
            operandShake.playAnimation();
            result.setText("Не введено одно из чисел");
        }
    }
}
