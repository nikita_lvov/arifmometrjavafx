package calculator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.getIcons().add(new Image("calculator\\assets\\icon.png"));
        Scene scene = new Scene(root, 500, 600);
        scene.getStylesheets().add(getClass().getResource("styles\\Styles.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.setTitle("Калькулятор");
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
